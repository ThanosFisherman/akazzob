package cliff.com.akazoob.core;

import java.util.ArrayList;

import cliff.com.akazoob.constant.Constant;

public class Horse {

    // Listener When Finish all Calculations starts to draw on Canvas.
    private doCalculateListener ml;
    // An ArrayList that saves the Paths. Results of Calculation
    private static ArrayList<Point> pointsArray;

    public Horse(doCalculateListener ml) {
        this.ml = ml;
    }


    /**
     * Initialize the Array. Creates the Start / End points and call the Calculation Methods
     */
    public void setupAndRun(int startX, int StartY, int EndX, int EndY) {
        pointsArray = new ArrayList<>();
        Point start = new Point(startX, StartY);
        Point end = new Point(EndX, EndY);
        calculate(start, Constant.MAXCOUNTER, null);
        findMatch(start, end);
        ml.callback(pointsArray);
    }

    /**
     * Recursion that Calculate the Next Possible moves of Horse
     */
    private static void calculate(Point point, int count, Point previus) {
        if (count < 0) {
            point.setNextPoint(null);
            point.setPreviousPoint(previus);
        } else {
            for (int[] o : Constant.offsets) {
                point.addNextPoint(new Point((point.getPointX() + o[0]), (point.getPointY() + o[1])));
                point.setPreviousPoint(previus);
                point.setStep(count);
            }
            count--;
            for (int i = 0; i < point.getNextPoint().size(); i++) {
                calculate(point.getNextPoint().get(i), count, point);
            }
        }
    }

    /**
     * Recursion tha Calculate if Horse can move to End Point on 3 Moves
     */
    private static void findMatch(Point data, Point end) {
        for (int i = 0; i < data.getNextsize(data.getNextPoint()); i++) {
            if (!data.getNextPoint().get(i).equals(end)) {
                findMatch(data.getNextPoint().get(i), end);
            } else if (data.getNextPoint().get(i).equals(end)) {
                pointsArray.add(data.getNextPoint().get(i));
            }
        }
    }

    public interface doCalculateListener {
        void callback(ArrayList<Point> points);
    }


}