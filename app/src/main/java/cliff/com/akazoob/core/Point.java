package cliff.com.akazoob.core;

import java.util.ArrayList;

import cliff.com.akazoob.constant.Constant;

/**
 * Created by cliff on 26/9/2016.
 */

public class Point {

    /**
     * Point
     * X,Y , NextPoint , PreviousPoint
     */
    private int pointX;
    private int pointY;
    private int step;
    private ArrayList<Point> nextPoint;
    private Point previousPoint;

    Point() {
    }

    /**
     * Constructor
     */
    Point(int pointX, int pointY) {
        nextPoint = new ArrayList<>();
        this.pointX = pointX;
        this.pointY = pointY;
        previousPoint = null;
    }


    /**
     * Getter
     */
    public int getPointX() {
        return pointX;
    }

    public int getPointY() {
        return pointY;
    }

    /**
     * Getter on Step Counter
     */
    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    /**
     * Returns the Next point of point
     */
    public ArrayList<Point> getNextPoint() {
        return nextPoint;
    }

    /**
     * Check if Next size if null of empty
     */
    public int getNextsize(ArrayList<Point> xSize) {
        if (xSize == null) {
            return 0;
        } else {
            return xSize.size();
        }
    }

    /**
     * Check if the Point is out of Board
     */
    public static boolean isOutOfBoard(Point point) {
        return point.getPointX() > Constant.ROWS - 1 || point.getPointY() > Constant.COLS - 1 || point.getPointX() < 0 || point.getPointY() < 0;
    }

    /**
     * set ArrayList of Point to nextPoint
     */
    public void setNextPoint(ArrayList<Point> nextPoint) {
        this.nextPoint = nextPoint;
    }

    /**
     * set Point of Point to nextPoint
     */
    public void addNextPoint(Point point) {
        if (!isOutOfBoard(point))
            nextPoint.add(point);
    }
    /**
     * getter for the PreviousPoint
     */
    public Point getPreviousPoint() {
        return previousPoint;
    }

    /**
     * set Point of Point to previousPoint
     */
    public void setPreviousPoint(Point previusPoint) {
        this.previousPoint = previusPoint;
    }

    /**
     * Compare 2 Points
     */
    public boolean equals(Point o) {
        return o != null && pointX == o.getPointX() && pointY == o.getPointY();
    }


    @Override
    public String toString() {
        return "Point{" +
                "pointX=" + pointX +
                ", pointY=" + pointY +
                '}';
    }

}
