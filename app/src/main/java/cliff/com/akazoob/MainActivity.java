package cliff.com.akazoob;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import cliff.com.akazoob.chess.Chessboard;
import cliff.com.akazoob.core.Horse;
import cliff.com.akazoob.core.Point;

public class MainActivity extends Activity implements View.OnClickListener, Chessboard.Listener, Horse.doCalculateListener {

    private Button calc;
    private Button reset;
    private Chessboard chess;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        calc = (Button) findViewById(R.id.calc);
        reset = (Button) findViewById(R.id.reset);
        chess = (Chessboard) findViewById(R.id.chess);
        chess.setupButtonListener(this);
        calc.setOnClickListener(this);
        reset.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.calc:
                //Log.e("OnClick","Start"+" X :"+chess.getStartPoint().getposX()+" Y :"+chess.getStartPoint().getposY());
                // Log.e("OnClick","END"+" X :"+chess.getEndPoint().getposX()+" Y :"+chess.getEndPoint().getposY());
                Horse horse = new Horse(this);
                horse.setupAndRun(chess.getStartPoint().getposX(), chess.getStartPoint().getposY(), chess.getEndPoint().getposX(), chess.getEndPoint().getposY());
                break;
            case R.id.reset:
                // Log.e("OnClick", "onClick: RESET");
                chess.resetCanvas();
                changeStatusButtonCalc(false);
                break;
        }
    }

    /**
     * When user Select Start/End Tile this method change the status of Buttons
     */
    @Override
    public void changeStatusButtonCalc(Boolean status) {
        if (status) {
            reset.setVisibility(View.VISIBLE);
            calc.setVisibility(View.VISIBLE);
        } else {
            reset.setVisibility(View.INVISIBLE);
            calc.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Callback method when finish all calculations and returns an array of Points
     * if Array of points is empty or null (no path found) alert the user
     */
    @Override
    public void callback(ArrayList<Point> points) {
        Log.e("callback", ": points size :" + points.size());
        if (points != null && points.size() >= 1) {

            for (int i = 0; i < points.size(); i++) {
                chess.changeColorOfLine();
                chess.setPointToStartPointToDrawLine(points.get(i).getPointX(), points.get(i).getPointY());
                chess.setPointsToDrawLine(points.get(i));
            }

        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Path not found")
                    .setMessage("Please change the selected Start/End tiles")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                        }
                    })
                    .show();
        }
    }

}
