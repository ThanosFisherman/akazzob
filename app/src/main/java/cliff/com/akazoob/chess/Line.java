package cliff.com.akazoob.chess;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by cliff on 26/9/2016.
 */

public class Line extends View {

    private Paint paint;
    private Path path;

    public Line(Context context) {
        super(context);
        setFocusable(true);
        init();
    }

    public Line(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Line(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public Line(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    /**
     * Initialize the Line
     */
    private void init(){
        this.paint = new Paint();
        this.path = new Path();
        paint.setStyle(Paint.Style.STROKE);
        this.paint.setColor(Color.GREEN);
        this.paint.setStrokeWidth(10);
    }

    /**
     * Draw the Line with Path and Color etc on Canvas
     */
    public void draw(final Canvas canvas) {
        canvas.drawPath(path, paint);
        invalidate();
    }

    /**
     * add Line
     */
    public void addpath(float x , float j) {
        path.lineTo(x, j);
    }

    /**
     * move Line
     */
    public void addStartPath(float x , float j){
        path.moveTo(x,j);

    }

    /**
     * Change color and invalidate
     */
    public void changeColor(int color){
        this.paint.setColor(color);
        invalidate();
    }

    public void restPath(){
        path.reset();
    }
}
