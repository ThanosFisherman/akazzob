package cliff.com.akazoob.chess;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by cliff on 25/9/2016.
 */

public class MyPoint extends View  {

    private Rect rect =new Rect(0,0,100,100);
    private  Paint paint;
    private boolean isSelected=false;
    private int xPos;
    private int yPos;

    public MyPoint(Context context) {
        super(context);
        init();
    }

    public MyPoint(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyPoint(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    /**
     * Check if Point is Selected . When user Tap on Tile this method check if is Selected
     */
    @Override
    public boolean isSelected() {
        return isSelected;
    }

    @Override
    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    /**
     * Get instance of Rect
     */
    public Rect getRect() {
        return rect;
    }

    public void setRect(Rect rect) {
        this.rect = rect;
    }

    /**
     * Get X position of Point
     */
    public int getposX() {
        return xPos;
    }

    public void setX(int x) {
        this.xPos = x;
    }

    /**
     * Get Y position of Point
     */
    public int getposY() {
        return yPos;
    }

    public void setY(int y) {
        this.yPos = y;
    }

    /**
     * Get X/Y position of Point
     */
    public void setposXY(int x, int y) {
        this.xPos=x;
        this.yPos = y;
    }

    /**
     * Initialize of Point
     */
    private void init(){
        this.paint = new Paint();
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setColor(Color.WHITE);
        this.paint.setStrokeWidth(6);
    }

    public void draw(final Canvas canvas) {
        canvas.drawRect(rect, paint);
        invalidate();
    }

    /**
     * On User Taps on Chess Change Place and Color (Green/Red)
     */
    public void changePositionAndColor(Rect rect,int Color){
        this.rect.set(rect);
        this.paint.setColor(Color);
        invalidate();
    }

    /**
     * Reset.. Change Color to White and Position to 0,0 of Screen
     */
    public void resetPositionAndColor(){
        this.rect.set(new Rect(0,0,0,0));
        this.paint.setColor(Color.WHITE);
        invalidate();
    }
}
