package cliff.com.akazoob.chess;

/**
 * Created by cliff on 25/9/2016.
 */

import android.content.Context;
import android.graphics.Canvas;

import android.graphics.Color;
import android.graphics.Rect;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;


import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.Random;

import cliff.com.akazoob.constant.Constant;
import cliff.com.akazoob.core.Point;


public final class Chessboard extends View {
    private static final String TAG = Chessboard.class.getSimpleName();

    private final Tile[][] mTiles;

    private int x0 = 0;
    private int y0 = 0;
    private int squareSize = 0;

    private final MyPoint startPoint;
    private final MyPoint endPoint;
    private Line line;
    Listener ml;

    /**
     * Getter user seleted Tile
     */
    public MyPoint getStartPoint() {
        return startPoint;
    }

    public MyPoint getEndPoint() {
        return endPoint;
    }

    /**
     * 'true' if black is facing player.
     */
    private boolean flipped = false;

    /**
     * Constructor
     */
    public Chessboard(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        this.mTiles = new Tile[Constant.COLS][Constant.ROWS];
        startPoint = new MyPoint(this.getContext());
        endPoint = new MyPoint(this.getContext());
        line = new Line(this.getContext());
        setFocusable(true);
        buildTiles();
    }

    /**
     * Listener
     */
    public void setupButtonListener(Listener ml) {
        this.ml = ml;
    }

    private void buildTiles() {
        for (int c = 0; c < Constant.COLS; c++) {
            for (int r = 0; r < Constant.ROWS; r++) {
                mTiles[c][r] = new Tile(c, r);
            }
        }
    }


    /**
     * onDraw
     */
    @Override
    protected void onDraw(final Canvas canvas) {
        final int width = getWidth();
        final int height = getHeight();

        this.squareSize = Math.min(
                getSquareSizeWidth(width),
                getSquareSizeHeight(height)
        );

        computeOrigins(width, height);

        for (int c = 0; c < Constant.COLS; c++) {
            for (int r = 0; r < Constant.ROWS; r++) {
                final int xCoord = getXCoord(c);
                final int yCoord = getYCoord(r);

                final Rect tileRect = new Rect(
                        xCoord,               // left
                        yCoord,               // top
                        xCoord + squareSize,  // right
                        yCoord + squareSize   // bottom
                );

                mTiles[c][r].setTileRect(tileRect);
                mTiles[c][r].draw(canvas);
            }
        }
        startPoint.draw(canvas);
        endPoint.draw(canvas);
        line.draw(canvas);
        invalidate();
    }

    /**
     * Reset Canvas when user press Reset Button
     */
    public void resetCanvas() {
        startPoint.resetPositionAndColor();
        startPoint.setSelected(false);
        endPoint.resetPositionAndColor();
        endPoint.setSelected(false);
        line.restPath();
    }

    /**
     * When user Tap on ChessBoard
     */
    @Override
    public boolean onTouchEvent(final MotionEvent event) {


        int action = MotionEventCompat.getActionMasked(event);

        switch (action) {
            case (MotionEvent.ACTION_DOWN):
                final int x = (int) event.getX();
                final int y = (int) event.getY();

                Tile tile;
                for (int c = 0; c < Constant.COLS; c++) {
                    for (int r = 0; r < Constant.ROWS; r++) {
                        tile = mTiles[c][r];
                        if (tile.isTouched(x, y)) {
                            selectedChecker(c, r);
                        }
                    }
                }
                return true;
            default:
                return super.onTouchEvent(event);
        }
    }

    /**
     * Tap Selector
     */
    private void selectedChecker(int c, int r) {
            // Green =0 Red=0
        if (!startPoint.isSelected() && !endPoint.isSelected()) {
            startPoint.changePositionAndColor(mTiles[c][r].getTileRect(), Color.GREEN);
            startPoint.setSelected(true);
            startPoint.setposXY(c, r);
            ml.changeStatusButtonCalc(false);
            // Green =1 Red=0
        } else if (startPoint.isSelected() && !endPoint.isSelected()) {
            endPoint.changePositionAndColor(mTiles[c][r].getTileRect(), Color.RED);
            endPoint.setSelected(true);
            endPoint.setposXY(c, r);
            line.addStartPath(mTiles[c][r].getCenterX(), mTiles[c][r].getCenterY());
            ml.changeStatusButtonCalc(true);
            // Green =0 Red=1
        } else if (!startPoint.isSelected() && endPoint.isSelected()) {
            startPoint.changePositionAndColor(mTiles[c][r].getTileRect(), Color.GREEN);
            startPoint.setSelected(true);
            startPoint.setposXY(c, r);
            ml.changeStatusButtonCalc(false);
            // Green .equals Red
        } else if (startPoint.getRect().equals(endPoint.getRect())) {
            startPoint.resetPositionAndColor();
            startPoint.setSelected(false);
            endPoint.resetPositionAndColor();
            endPoint.setSelected(false);
            ml.changeStatusButtonCalc(false);
        } else {
            if (mTiles[c][r].getTileRect().equals(startPoint.getRect())) {
                Log.e(TAG, "selectedChecker: START");
                startPoint.resetPositionAndColor();
                startPoint.setSelected(false);
                ml.changeStatusButtonCalc(false);
            } else if ((mTiles[c][r].getTileRect().equals(endPoint.getRect()))) {
                Log.e(TAG, "selectedChecker: END");
                endPoint.resetPositionAndColor();
                endPoint.setSelected(false);
                ml.changeStatusButtonCalc(false);
            }
        }
    }

    /**
     * Get Square Size / 8 tiles
     */
    private int getSquareSizeWidth(final int width) {
        return width / 8;
    }

    private int getSquareSizeHeight(final int height) {
        return height / 8;
    }

    private int getXCoord(final int x) {
        return x0 + squareSize * (flipped ? 7 - x : x);
    }

    private int getYCoord(final int y) {
        return y0 + squareSize * (flipped ? y : 7 - y);
    }

    private void computeOrigins(final int width, final int height) {
        this.x0 = (width - squareSize * 8) / 2;
        this.y0 = (height - squareSize * 8) / 2;
    }

    /**
     * For each point get paths and draw Line
     */
    public void setPointsToDrawLine(Point x) {
        if (x != null) {
            Log.e(TAG, "setPointsToDrawLine:  X :" + x.getPointX() + " Y : " + x.getPointY());
            line.addpath(mTiles[x.getPointX()][x.getPointY()].getCenterX(), mTiles[x.getPointX()][x.getPointY()].getCenterY());
            if (x.getPreviousPoint() != null) {
                setPointsToDrawLine(x.getPreviousPoint());
            }

        }
    }

    /**
     * Moves the Starting Point of Line
     */
    public void setPointToStartPointToDrawLine(int c, int r) {
        line.addStartPath(mTiles[c][r].getCenterX(), mTiles[c][r].getCenterY());
    }

    /**
     * Gets Random color For the Line
     * Todo
     */
    public void changeColorOfLine() {
        Random rnd = new Random();
        line.changeColor(Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
    }

    public interface Listener {
         void changeStatusButtonCalc(Boolean status);
    }

}
