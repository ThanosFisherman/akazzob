package cliff.com.akazoob.chess;

/**
 * Created by cliff on 25/9/2016.
 */

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public final class Tile {
    private final int col;
    private final int row;
    private final Paint squareColor;
    private Rect tileRect;

    public Tile(final int col, final int row) {
        this.col = col;
        this.row = row;
        this.squareColor = new Paint();
        squareColor.setColor(isDark() ? Color.BLACK : Color.WHITE);
    }

    public void draw(final Canvas canvas) {
        canvas.drawRect(tileRect, squareColor);
    }

    public boolean isDark() {
        return (col + row) % 2 == 0;
    }

    public boolean isTouched(final int x, final int y) {
        return tileRect.contains(x, y);
    }

    public void setTileRect(final Rect tileRect) {
        this.tileRect = tileRect;
    }

    public Rect getTileRect() {
        return tileRect;
    }

    public int getCenterX(){
        return tileRect.centerX();
    }

    public int getCenterY(){
        return tileRect.centerY();
    }

}