package cliff.com.akazoob.constant;

/**
 * Created by cliff on 26/9/2016.
 */

public class Constant {

    public static final int COLS = 8;
    public static final int ROWS = 8;
    public static final int MAXCOUNTER = 2;

    public static int[][] offsets = {
            {-2, 1},
            {-1, 2},
            {1, 2},
            {2, 1},
            {2, -1},
            {1, -2},
            {-1, -2},
            {-2, -1}
    };
}
